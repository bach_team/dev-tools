<?php
/**
 * THIS FILE FOR AUTHORIZED DEVELOPERS ONLY!
 *
 * THESE ARE USEFUL TOOLS FOR MAGENTO.
 * DO NOT MODIFY CODE UNTIL YOU KNOW WHAT ARE YOU DOING.
 *
 * THIS FILE SHOULD BE CHANGED PASSWORD OR REMOVE AFTER DEVELOPMENT. WE DO NOT RESPONSE TO ANY RESULT.
 *
 */
require_once 'app/Mage.php';

class Dev
{
    const USERNAME = 'admin';
    const PASSWORD = 'admin!@#';
    public $storeId;

    /**
     * Initial variables
     */
    public function __construct()
    {
        $storeId        = Mage::app()->getRequest()->get('store_id');
        $this->_storeId = $storeId ? $storeId : Mage::app()->getStore()->getId();
        $this->initAction();
    }


    /**
     * Authentication user
     */
    protected function _authorize()
    {
        if (!isset($_COOKIE['isLoggedIn']) AND $this->_isLocal() AND ($_SERVER['PHP_AUTH_PW'] != self::PASSWORD || $_SERVER['PHP_AUTH_USER'] != self::USERNAME) || !$_SERVER['PHP_AUTH_USER']) {
            header('WWW-Authenticate: Basic realm="Test auth"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'Auth failed';
            exit;
        }
        setcookie('isLoggedIn', 1, time() + 86400 * 36500, '/');
    }

    /**
     * Check is local environment
     *
     * @return bool
     */
    protected function _isLocal()
    {
        return (in_array($_SERVER['SERVER_NAME'],
            array(
                'localhost', 'localhost.com', 'www.localhost.com', 'm.com', 'test.loc', 'mage.loc',
            )
        ));
    }

    /**
     * Magento and PHP information
     */
    public function getBasicConfigs()
    {
        echo '<span class="button">Note</span> <a href="' . $this->getUrl(array('action' => 'backup')) . '">Run Backup before making changes</a>';
        echo '<hr />';
        $configs = array(
            'web/unsecure/base_url',
            'web/secure/base_url',
            'design/head/default_title',
            'design/package/name',
            'design/theme/template',
            'design/theme/skin',
            'design/theme/layout',
            'design/head/default_robots',
            'design/header/logo_src',
            'general/locale/code',
            'design/header/welcome',
            'design/footer/copyright',
            'customer/account_share/scope',

            'dev/restrict/allow_ips',
            'dev/log/active',
            'dev/debug/template_hints',
            'dev/debug/template_hints_blocks',
            'dev/translate_inline/active',
            'admin/security/use_form_key',
            'admin/security/session_cookie_lifetime',
        );
        $storeId = $this->_storeId;
        echo '<div class="basic-config-wrapper" style="height: 450px;">';
        echo '<div style="width: 50%;float:left">';
        echo '<span><strong>General Config:</strong></span><br />';
        foreach ($configs as $config) {
            $value = $this->getStoreConfig($config, $storeId);
            echo '<span class="info"><span class="config-name"><a href="' . $this->getUrl(array('action' => 'setStoreConfig',
                                                                                                'path'   => urlencode($config),
                                                                                                'value'  => $value
                )) . '">' . $config . '</a></span>
		: ' . $value . '</span><br>';
        }
        echo '</div>';//End div left
        echo '<div style="width:50%;float:left;height: 100%;overflow-y:scroll; ">';
        echo '<span><strong>Installed Extensions:</strong></span><br />';
        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        foreach ($modules as $moduleName) {
            if ($moduleName === 'Mage_Adminhtml' || strpos($moduleName, 'Mage_') !== false) {
                continue;
            }
            $path  = 'advanced/modules_disable_output/' . $moduleName;
            $value = $this->getStoreConfig($path) == 0 ? 'Enabled' : 'Disabled';
            $color = $this->getStoreConfig($path) == 0 ? 'green' : 'red';
            echo '<span><a href="' . $this->getUrl(array('action' => 'setStoreConfig',
                                                         'path'   => urlencode($path),
                                                         'value'  => (int)$this->getStoreConfig($path)
                )) . '">' . $moduleName . '</a><strong style="color:' . $color . '"> (' . $value . ')</strong></span><br />';
        }
        echo '</div>';//End div right
        echo '</div>';
        echo '<div style="clear: both"></div>';

    }

    /**
     * get store config value
     *
     * @param      $config
     * @param null $storeId
     * @return mixed
     */
    public function getStoreConfig($config, $storeId = null)
    {
        return Mage::getStoreConfig($config, $storeId);
    }

    /**
     * @param      $path
     * @param      $value
     * @param null $storeId
     */
    public function setStoreConfig()
    {
        $storeId = $this->_storeId;
        $path    = isset($_GET['path']) ? $_GET['path'] : null;
        $value   = isset($_GET['value']) ? $_GET['value'] : null;
        if (isset($path) && isset($value)) {
            $this->saveConfig($path, $value, $storeId);
        }
    }


    /**
     * get Dev Url
     *
     * @param array $params
     * @return string
     */
    public function getUrl($params = array())
    {
        $routePath = basename(__FILE__);
        $cnt       = 0;
        foreach ($params as $key => $value) {
            if (is_null($value) || false === $value || '' === $value || !is_scalar($value)) {
                continue;
            }
            $cnt++;
            if ($cnt == 1) {
                $routePath .= '?' . $key . '=' . $value;
            } else {
                $routePath .= '&&' . $key . '=' . $value;
            }
        }

        return $routePath;
    }

    /**
     * @return mixed
     */
    public function initAction()
    {
        $action = $this->getAction();
        if ($action) {
            return $this->$action();
        }
    }

    /**
     * get action from request Url
     *
     * @return string
     */
    public function getAction()
    {
        return isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
    }

    /**
     * Enable frontend template path hint
     */

    public function frontendPH()
    {

        $storeId = $this->_storeId;
        //Allow current IP
        $allowed_ips = $this->getStoreConfig('dev/restrict/allow_ips', $storeId);
        $ips         = array_unique(array_merge(explode(',', $allowed_ips), array($_SERVER['REMOTE_ADDR'])));
        $allowed_ips = empty($allowed_ips) ? $_SERVER['REMOTE_ADDR'] : implode(",", $ips);
        $this->saveConfig('dev/restrict/allow_ips', $allowed_ips, 'stores', $storeId); //Save config
        //Toggle
        $config = $this->getStoreConfig('dev/debug/template_hints', $storeId);
        if ($config == 0) {
            $this->addMessage('<strong>Enabled</strong> template path hints', 1);
            $this->saveConfig('dev/debug/template_hints', 1, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 1, $storeId);

        } else {
            $this->saveConfig('dev/debug/template_hints', 0, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 0, $storeId);
            $this->addMessage('<strong>Disabled</strong> template path hints', 2);
        }
        // Refresh the config.
    }

    /**
     * Enable backend template path hint
     */
    public function backendPH()
    {
        $storeId = 0;//Admin store
        //Allow current IP
        $allowed_ips = $this->getStoreConfig('dev/restrict/allow_ips', $storeId);
        $ips         = array_unique(array_merge(explode(',', $allowed_ips), array($_SERVER['REMOTE_ADDR'])));
        $allowed_ips = empty($allowed_ips) ? $_SERVER['REMOTE_ADDR'] : implode(",", $ips);
        $this->saveConfig('dev/restrict/allow_ips', $allowed_ips, 'stores', $storeId);
        $config = $this->getStoreConfig('dev/debug/template_hints', $storeId);
        if ($config == 0) {
            $this->saveConfig('dev/debug/template_hints', 1, $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 1, $storeId);
            $this->addMessage('<strong>Enabled</strong> Admin template path hints', 1);
        } else {
            $this->saveConfig('dev/debug/template_hints', 0, 'stores', $storeId);
            $this->saveConfig('dev/debug/template_hints_blocks', 0, $storeId);
            $this->addMessage('<strong>Disabled</strong> Admin template path hints', 2);
        }
    }

    /**
     * Add message to session
     *
     * @param null $msg
     */
    public function addMessage($msg = null, $type = 0)
    {
        if (is_string($msg)) {
            $_SESSION['message'] .= $msg . '<br>';
            if ($type == 1) {
                $_SESSION['message_type'] = 'success-message';
            } else if ($type == 2) {
                $_SESSION['message_type'] = 'notice-message';
            } else {
                $_SESSION['message_type'] = 'default-message';
            }
        }
    }


    /**
     * @param      $path
     * @param null $storeId
     */
    public function saveConfig($path, $value, $storeId = null, $scope = 'stores')
    {
        Mage::getModel('core/config')->saveConfig($path, $value, $scope, $storeId);
        Mage::app()->getStore()->resetConfig();
    }

    /**
     * Flush Magento Cache
     *
     */
    public function flushCache()
    {
        Mage::app()->getCacheInstance()->flush();
        $this->addMessage('Flushed cache successfully at ' . date("Y-m-d H:i:s"), 1);
    }

    /**
     * Login as admin
     */
    public function loginAsAdmin()
    {
        Mage::getSingleton('core/session', array('name' => 'adminhtml'));
        $user = Mage::getModel("admin/user")->load(isset($_GET['id']) ? $_GET['id'] : 1);
        if (!isset($_GET['id'])) {
            $user = Mage::getModel('admin/user')->getCollection()->getFirstItem();
        } else if (!$user OR !$user->getId()) {

            echo('<strong>User does not exist</strong>');

            $users = Mage::getModel('admin/user')->getCollection();
            echo '<ol>';
            foreach ($users as $_user) {
                echo '<li><a target="_blank" href="' . $this->getUrl(array('action' => 'loginAsAdmin', 'id' => $_user->getId())) . '">login As ' . $_user->getUsername() . '</a></li>';
            }
            echo '</ol>';
            die;
        }

        if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
            Mage::getSingleton('adminhtml/url')->renewSecretUrls();
        }

        $session = Mage::getSingleton('admin/session');
        $session->setIsFirstVisit(true);
        $session->setUser($user);
        $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());

        Mage::dispatchEvent('admin_session_user_login_success', array('user' => $user));

        if ($session->isLoggedIn()) {

            $redirectUrl = Mage::getSingleton('adminhtml/url')->getUrl(Mage::getModel('admin/user')->getStartupPageUrl(), array('_current' => false));
            header('Location: ' . $redirectUrl);
            exit;
        }
    }

    /**
     * Login as customer
     *
     * @throws Mage_Core_Exception
     */
    public function loginAsCustomer()
    {
        Mage::getSingleton("core/session", array("name" => "frontend"));

        $websiteId = Mage::app()->getWebsite()->getId();
        $store     = Mage::app()->getStore();
        $customer  = Mage::getModel("customer/customer");
        $customer->setWebsiteId($websiteId);
        $customer->setStore($store);
        $customer->load(isset($_GET['id']) ? $_GET['id'] : 1);
        if (!$customer OR !$customer->getId()) {
            $customers = Mage::getModel('customer/customer')
                ->getCollection()
                ->setPageSize(100)
                ->setCurPage(1);

            $message = '<p><strong>User does not exist! Select a customer to login</strong></p>';
            $message .= '<ol>';
            foreach ($customers as $_customer) {
                $message .= '<li><a target="_blank" href="' . $this->getUrl(array('action' => 'loginAsCustomer', 'id' => $_customer->getId())) . '"> ' . $_customer->getEmail() . ' - Group: <strong>' . Mage::getModel('customer/group')->load($_customer->getGroupId())->getCustomerGroupCode() . '</strong> </a></li>';
            }
            $message .= '</ol>';
            $this->addMessage($message);

            return;
        }

        Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
        header('Location: ' . Mage::getBaseUrl(true));

        return;
    }

    /**
     * Enable/Disable log
     */
    public function enableLog()
    {
        $storeId = $this->_storeId;
        $this->toggleConfig('dev/log/active', $storeId);
    }

    /**
     * @param        $name
     * @param null   $storeId
     * @param string $stores
     */
    public function toggleConfig($name, $storeId = null, $scope = 'stores')
    {

        $config = $this->getStoreConfig($name, $storeId);
        if ($config == 0) {
            $this->addMessage('<strong>Enabled</strong> ' . $name, 1);
            $this->saveConfig($name, 1, $storeId, $scope);

        } else {
            $this->saveConfig($name, 0, $storeId, $scope);
            $this->addMessage('<strong>Disabled</strong> ' . $name, 2);
        }
    }

    /**
     * Backup app, skin, js folder
     */
    public function backup()
    {
        $file    = 'backup-magegiant-' . date("Y-m-d");
        $cmd     = "tar -czvf $file.tgz skin/ js/ app/";
        $message = "<pre>";
        $message .= "IN  : " . realpath(__FILE__) . "\n";
        $message .= "EXEC: $cmd \n";
        $message .= "php_user@server:\n" . shell_exec('pwd');
        system($cmd . ' &');
        $message .= "Done!</pre>";
        $this->addMessage($message, 1);
    }

    /**
     * Enable/Disable cache
     */
    public function cache()
    {
        Mage::app('admin');
        Mage::app()->cleanAllSessions();
        Mage::app()->getCacheInstance()->flush();
        Mage::app()->cleanCache();

        $types = Array(
            0 => 'config',
            1 => 'layout',
            2 => 'block_html',
            3 => 'translate',
            4 => 'collections',
            5 => 'eav',
            6 => 'config_api',
            7 => 'fullpage',
            8 => 'config_api2'
        );

        $allTypes     = Mage::app()->useCache();
        $updatedTypes = 0;
        $mode         = isset($_GET['mode']) ? $_GET['mode'] : 1;
        foreach ($types as $code) {
            if (array_key_exists($code, $allTypes)) {
                $allTypes[$code] = $mode;
                $updatedTypes++;
            }

            Mage::app()->getCacheInstance()->cleanType($code);
        }
        if ($updatedTypes > 0) {
            Mage::app()->saveUseCache($allTypes);
            if ($mode) {
                $this->addMessage("Cache Enabled Programmatically", 1);
            } else {
                $this->addMessage("Cache Disabled Programmatically", 2);
            }
        }
    }

    /**
     * @param $fromDir
     * @param $toFile
     */
    public function zip($fromDir, $toFile)
    {
        $file    = $toFile;
        $cmd     = "tar -czvf $file.tgz " . $fromDir;
        $message = "<pre>";
        $message .= "IN  : " . realpath(__FILE__) . "\n";
        $message .= "EXEC: $cmd \n";
        $message .= "php_user@server:\n" . shell_exec('pwd');
        system($cmd . ' &');
        $message .= "Done!</pre>";
        $this->addMessage($message, 1);
    }

    /**
     * Zip current dir
     */
    public function zipCurrent()
    {
        $this->zip(getcwd(), $_SERVER['HTTP_HOST']);
    }

    /**
     * Zip code
     */
    public function zipCode()
    {
        $ouput_file    = 'patch_' . $_SERVER['HTTP_HOST'] . '.zip';
        $media_exclude = array('.', '..', 'media', 'catalog', 'wysiwyg', 'xmlconnect', 'downloadable', 'dhl', 'customer', 'var', '_tmp', 'tmp', '.htaccess');
        if (isset($_GET['lite'])) {
            $lib_exclude = array('.', '..', '3Dsecure', 'captcha', 'flex', 'googlecheckout', 'LinLibertineFont', '', 'Mage', 'PEAR', 'phpseclib', 'Varien', 'Zend');
        } else {
            $lib_exclude = array('.', '..');
        }

        $folders = array(
            'app/etc/modules/',
            'app/locale/',
            'app/code/local/',
            'app/code/community/',
            'app/design/adminhtml/default/',
            'app/design/frontend/base/',
            'app/design/frontend/default/',
            'app/design/frontend/enterprise/',
            'js/',
            'skin/adminhtml/base/',
            'skin/adminhtml/default/',
            'skin/frontend/base/',
            'skin/frontend/default/',
            'skin/frontend/enterprise/',
            'shell/',
        );

        $media = scandir('media');
        foreach ($media as $k => $v) {
            if (in_array($v, $media_exclude) OR is_file($v)) {
                unset($media[$k]);
            } else {
                $media[$k] = 'media' . '/' . $v . '/';
            }
        }

        $lib = scandir('lib');
        foreach ($lib as $k => $v) {
            if (in_array($v, $lib_exclude) OR is_file($v)) {
                unset($lib[$k]);
            } else {
                $lib[$k] = 'lib' . '/' . $v . '/';
            }
        }


        $folders = array_merge($folders, $media, $lib);
        try {
            $zip = new ZipArchive();
            if ($zip->open($ouput_file, ZIPARCHIVE::CREATE) == true) {
                //Files
                foreach ($folders as $folder) {
                    if (is_dir($folder)) {
                        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder));
                        foreach ($iterator as $key => $value) {
                            if (is_file($key)) {
                                $zip->addFile(realpath($key), $key);
                            } else {
                                $zip->addEmptyDir(realpath($key));

                            }

                        }
                    }

                }
            }
            $zip->close();
        } catch (Exception $e) {
            $this->addMessage($e->getMessage(), 2);
        }
        $this->addMessage(' ' . date('Y-m-d H:i:s') . " Archive created successfully. File is: " . '<a href="' . $ouput_file . '" target="_blank">' . $ouput_file . '</a>', 1);
    }

    /**
     * Zip all media files
     */
    public function zipMedia()
    {
        $this->zip('media/', 'media');
    }

    /**
     * Show all config
     */
    public function showConfig()
    {
        header("Content-Type: text/xml");
        die(Mage::app()->getConfig()->getNode()->asXML());
    }

    /**
     *
     */
    public function showLayout()
    {
        header('Content-Type: text/xml');
    }

    /**
     * Unzip a file
     */
    public function unzip()
    {
        $file = $_REQUEST['file'];
        if (isset($file)) {
            $zip = new ZipArchive;
            $res = $zip->open($file);
            if ($res === true) {
                $zip->extractTo(getcwd());
                $zip->close();
                $this->addMessage('File ' . $file . ' is extracted to ' . getcwd());
            } else {
                $this->addMessage('Can\'t unzip ' . $file . ' file', 2);
            }
        }
    }

    /**
     * View php information
     */
    public function phpInfo()
    {
        phpinfo();
    }

    /**
     * Process reindex
     */
    public function reindex()
    {
        $indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection();
        foreach ($indexingProcesses as $process)
            $process->reindexEverything();
        $this->addMessage('Reindex successfully', 1);
    }

    /**
     * Enable/Disable frontend translate inline
     */
    public function toggleTranslateInline()
    {
        $this->toggleConfig('dev/translate_inline/active', $this->_storeId);
    }

    /**
     * Enable/Disable backend translate inline
     */

    public function toggleTranslateInlineAdmin()
    {
        $this->toggleConfig('dev/translate_inline/active_admin', $this->_storeId);
    }

    /**
     * Delete a folder
     */
    public function deleteFolder($dirPath = null)
    {
        if (!$dirPath) {
            $dirPath = isset($_REQUEST['folder']) ? $_REQUEST['folder'] : '';
            if (!$dirPath)
                return;
        }
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteFolder($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    /**
     * @return bool
     */
    public function deleteFile()
    {
        if (isset($_GET['file']) && is_file($_GET['file'])) {
            try {
                unlink($_GET['file']);
                $this->addMessage($_GET['file'] . ' is deleted', 1);
            } catch (Exception $e) {
                $this->addMessage('Can\'t delete ' . $_GET['file'], 2);
            }
        } else {
            $this->addMessage('Can\'t found ' . $_GET['file'], 2);
        }
    }

    /**
     * Run php command
     */
    public function run()
    {
        if (isset($_GET['cmd'])) {
            eval($_GET['cmd']);
        }
    }

    /**
     * Run a query
     */
    public function sqlQuery()
    {
        if (isset($_REQUEST['cmd'])) {
            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            zend_debug::dump($write->query($_REQUEST['cmd']));
        }
    }

    public function getAllStoreConfigs()
    {
        $model   = new Mage_Core_Model_Config_Data;
        $configs = $model->getCollection();
        $message = '<ul>';
        foreach ($configs as $config) {
            $message .= '<li>';
            $message .= '#' . $config->getId() . ' - Scope: ' . $config->getScope() . ' - <strong><a href="' . $this->getUrl(array('action' => 'setStoreConfig',
                                                                                                                                   'path'   => urlencode($config->getPath()),
                                                                                                                                   'value'  => $config->getValue())) . '">' . $config->getPath() . '</a></strong> : ';
            if (@unserialize($config->getValue()) !== false) {
                $message .= 'Serialize Data';
            } else {
                $message .= $config->getValue();
            }

            $message .= '</li>';
        }
        $message .= '</ul>';
        $this->addMessage($message);
    }

    /**
     * Set cookie life time
     */
    public function adminCookieLifeTime()
    {
        $this->saveConfig('admin/security/session_cookie_lifetime', isset($_GET['value']) ? $_GET['value'] : 86400 * 365, 0, 'default');
    }

    /**
     * Use admin form key
     */
    public function toggleAdminUseFormKey()
    {
        $this->toggleConfig('admin/security/use_form_key', $this->_storeId);
    }

    /**
     * Merge css config
     */
    public function toggleMergeCss()
    {
        $this->toggleConfig('dev/css/merge_css_files', $this->_storeId);
    }

    /**
     * Merge js config
     */
    public function toggleMergeJs()
    {
        $this->toggleConfig('dev/js/merge_files', $this->_storeId);
    }

    /**
     * Self remove
     */
    public function selfRemove()
    {
        unlink(basename(__FILE__));
        $this->addMessage("Dev file is removed successfully!", 1);
    }
}

$dev = new Dev();
?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Dev tools</title>
    <link rel="icon" href="<?php echo Mage::getDesign()->getSkinUrl('favicon.ico') ?>" type="image/x-icon">
    <style type="text/css">
        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        a {
            text-decoration: none;
            font-weight: 500;
            color: #563d7c;
        }

        .list {
            list-style-type: decimal;
        }

        .list li {
            margin-bottom: 0.2em;
        }

        .success-message {
            transition-duration: 0.3s;
            background: #7db500;
            color: white;
            text-shadow: none;
            padding: 0.5em;
        }

        .notice-message {
            transition-duration: 0.3s;
            background: #b5140a;
            color: white;
            text-shadow: none;
            padding: 0.5em;
        }

        .button {
            background: #00a1cb;
            color: white;
            text-shadow: none;
            /* border: none; */
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-transition-property: background;
            -moz-transition-property: background;
            -o-transition-property: background;
            transition-property: background;
            -webkit-transition-duration: 0.3s;
            -moz-transition-duration: 0.3s;
            -o-transition-duration: 0.3s;
            transition-duration: 0.3s;
            padding: 3px;
        }

        .nav {
            width: 25%;
            float: left;
            margin-left: 5%;
        }

        .content {
            width: 60%;
            float: left;
            margin-top: 2%;
            margin-right: 5%;
        }

        .content form {
            margin-bottom: 10px;
        }

        .config-name {
            font-weight: bold;
        }

        .input-box label {
            display: block;
            font-weight: bold;
        }

        .input-box input, .input-box textarea {
            width: 350px;
        }

        .button-box {
            margin-top: 5px;
        }

        .message {
            margin-bottom: 10px;
        }

        .footer-container {
            padding: 30px;
        }

        .footer address {
            border-top: 1px solid #cccccc;
            text-align: center;
            width: 100%;
            font-size: 11px;
            margin-top: 30px;
            padding: 30px 0;
            font-style: normal;
            color: #3399cc;
            text-align: center;
        }
    </style>

</head>


<body>


<div class="nav" style="width: 25%;
	float: left;
	margin-left: 5%;
	">
    <h3>Navigation</h3>
    <ul class="list">
        <li><a href="<?php echo $dev->getUrl(); ?>">Dashboard</a></li>
        <li><a target="_blank" href="<?php echo Mage::getBaseUrl(true) ?>">Go
                to <?php echo Mage::getBaseUrl(true) ?></a></li>
        <li>
            <a class="button" href="<?php echo $dev->getUrl(array('action' => 'frontendPH')) ?>">Frontend </a> /
            <a class="button" href="<?php echo $dev->getUrl(array('action' => 'backendPH')) ?>">Backend TPH</a>
        </li>
        <li><a class="button" href="<?php echo $dev->getUrl(array('action' => 'flushCache')) ?>">Flush Cache</a></li>

        <li><a target="_blank" href="<?php echo $dev->getUrl(array('action' => 'loginAsAdmin')) ?>">Login As Admin</a> /
            <a
                target="_blank" href="<?php echo $dev->getUrl(array('action' => 'loginAsCustomer')) ?>">Customer</a>
        </li>

        <li><a href="<?php echo $dev->getUrl(array('action' => 'enableLog')) ?>">Enable Log</a></li>

        <li><a href="<?php echo $dev->getUrl(array('action' => 'backup')) ?>">Run Backup: app,js,skin</a></li>

        <li>
            <a href="<?php echo $dev->getUrl(array('action' => 'cache', 'mode' => 0)) ?>">Disable </a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'cache', 'mode' => 1)) ?>">Enable Cache</a>
        </li>
        <li>
            Zip:
            <a href="<?php echo $dev->getUrl(array('action' => 'zipCurrent')) ?>">Current</a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'zipCode')) ?>">Code</a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'zipMedia')) ?>">Media</a>
        </li>

        <li><a href="<?php echo $dev->getUrl(array('action' => 'showConfig')) ?>" target="_blank">Show Config</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'showLayout')) ?>" target="_blank">Show Layout</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'unzip')) ?>">Unzip file</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'phpInfo')) ?>" target="_blank">PHPInfo()</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'reindex')) ?>">Re-Index</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'toggleTranslateInline')) ?>">Frontend / <a
                    href="<?php echo $dev->getUrl(array('action' => 'toggleTranslateInlineAdmin')) ?>">Backend</a>
                Translate Inline</a>
        </li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'deleteFolder')) ?>">Delete Folder</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'deleteFile')) ?>">Delete File</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'run')) ?>">Run</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'sqlQuery')) ?>">Run sql Query</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'getAllStoreConfigs')) ?>">Show All Store Configs</a>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'setStoreConfig')) ?>">Set Store Config</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'adminCookieLifeTime')) ?>">Admin Cookie LifeTime - 365
                days</a></li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'toggleAdminUseFormKey')) ?>">Admin Use Form Key</a></li>
        <li>
            <a href="<?php echo $dev->getUrl(array('action' => 'toggleMergeCss')) ?>">Merge CSS</a> /
            <a href="<?php echo $dev->getUrl(array('action' => 'toggleMergeJs')) ?>">Js</a>
        </li>
        <li><a href="<?php echo $dev->getUrl(array('action' => 'selfRemove')) ?>">Self-Remove</a></li>


    </ul>


</div>

<div class="content" style="width: 60%;
	float: left;
	margin-top: 2%;
	margin-right: 5%;">

    <?php
    if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
        echo '<div class="message ' . (isset($_SESSION['message_type']) ? $_SESSION['message_type'] : 'message') . '">' . $_SESSION['message'] . '</div>';
        unset($_SESSION['message']);
        unset($_SESSION['message_type']);
    }
    ?>
    <?php if ($dev->getAction() == 'unzip') { ?>
        <form action="" method="POST">
            <input type="hidden" name="action" value="unzip"/>

            <div class="input-box">
                <input type="text" placeholder="Enter File.zip in current Dir" name="file" size="100">
            </div>
            <div class="button-box">
                <input type="submit" name="unzipnow" value="UNZIP now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'setStoreConfig') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="setStoreConfig"/>

            <div class="input-box">
                <label for="config_path">
                    Config Path
                </label>
                <input type="text" placeholder="Path" name="path" id="config_path"
                       value="<?php echo isset($_REQUEST['path']) ? $_REQUEST['path'] : '' ?>" size="100">
            </div>
            <div class="input-box">
                <label for="config_value">
                    Value
                </label>
                 <textarea placeholder="" cols="10" rows="3" id="config_value"
                           name="value"><?php echo isset($_REQUEST['value']) ? $_REQUEST['value'] : '' ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="saveStoreConfig" value="Save">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'deleteFolder') { ?>
        <form action="" method="POST">
            <input type="hidden" name="action" value="deleteFolder"/>

            <div class="input-box">
                <input type="text" placeholder="Enter Folder in current Dir" name="folder" size="100" value="">
            </div>
            <div class="button-box">
                <input type="submit" name="deleteFolderNow" value="DELETE now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'deleteFile') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="deleteFile"/>

            <div class="input-box">
                <input type="text" placeholder="Enter file name" name="file" size="100" value="">
            </div>
            <div class="button-box">
                <input type="submit" name="deleteFileNow" value="DELETE now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'run') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="run"/>

            <div class="input-box">
                 <textarea placeholder="" cols="80" rows="10"
                           name="cmd"><?php echo isset($_GET['cmd']) ? $_GET['cmd'] : null ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="run" value="Run now">
            </div>
        </form>
    <?php } ?>

    <?php if ($dev->getAction() == 'sqlQuery') { ?>
        <form action="" method="GET">
            <input type="hidden" name="action" value="sqlQuery"/>

            <div class="input-box">
                 <textarea placeholder="" cols="80" rows="10"
                           name="cmd"><?php echo isset($_GET['cmd']) ? $_GET['cmd'] : null ?></textarea>
            </div>
            <div class="button-box">
                <input type="submit" name="sqlQuery" value="Run SQL">
            </div>
        </form>
    <?php } ?>
    <?php $dev->getBasicConfigs(); ?>
    Magento version <strong><?php echo Mage::getVersion() ?></strong> <br>
    Path: <strong><?php echo $_SERVER['SCRIPT_FILENAME'] ?></strong> <br>
    PHP: <strong><?php echo phpversion() ?></strong> <br>
    Server time: <strong><?php echo date("Y-m-d H:i:s", time()) ?></strong> <br>


</div>
<div style="clear: both"></div>
<div class="footer-container">
    <div class="footer">
        <address class="copyright">&copy; Bach Lee. Skype: bachlee89</address>
    </div>
</div>
</body>
</html>